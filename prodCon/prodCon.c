#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <memory.h>
#include "prodcon.h"
#include <threads.h>
#include <unistd.h>
#include <stdbool.h> 

static assign_consumer_f assign_consumer;
static run_producer_f run_producer;
static run_consumer_f run_consumer;

static int producer_count;
static int consumer_count;
static thread_local int my_consumer_number;

struct llist_node 
{
    struct llist_node *next;
    char *str;
    bool sentinal;
};

static struct llist_node **heads;
static pthread_mutex_t *lock;
static pthread_cond_t *cond;

struct prodarg_t
{
    int num;
    int prod_count;
    int argc;
    char **argv;
};

struct conarg_t
{
    int num;
    int argc;
    char **argv;
};

//given a char*, adds string to top of stack and signals the consumer of
//the corrisponding list, thread safe 
void produce(const char *buffer)
{
    int conNum = assign_consumer(consumer_count, buffer);
    pthread_mutex_lock(&lock[conNum]);
    struct llist_node *new_node = malloc(sizeof(struct llist_node));
    new_node -> next = heads[conNum];
    new_node -> str = strdup(buffer);
    new_node -> sentinal = false;
    heads[conNum] = new_node;
    pthread_cond_signal(&cond[conNum]);
    pthread_mutex_unlock(&lock[conNum]);
}

//if linked list is empty, waits for producer to send signal, 
//returns NULL if node is sentinal, otherwise pops top node and returns its str
char *consume()
{
    pthread_mutex_lock(&lock[my_consumer_number]);
    while(heads[my_consumer_number] == NULL)
    {
        pthread_cond_wait(&cond[my_consumer_number], &lock[my_consumer_number]);
    }
    if(heads[my_consumer_number] -> sentinal)
    {
        return NULL;
    }
    struct llist_node **phead = &heads[my_consumer_number];
    char *consumed = (*phead) -> str;
    struct llist_node *next = (*phead) -> next;
    free(*phead);
    *phead = next;
    pthread_mutex_unlock(&lock[my_consumer_number]);
    return consumed;
}

//starts producer 
void *startProd(void *arg)
{
    struct prodarg_t *prodArg = (struct prodarg_t *) arg;
    run_producer(prodArg -> num, producer_count, produce, prodArg -> argc, prodArg -> argv);
}

//starts consumer
void *startCon(void *arg)
{
    struct conarg_t *conArg = (struct conarg_t *) arg;
    my_consumer_number = conArg -> num;
    run_consumer(conArg -> num, consume, conArg -> argc, conArg -> argv);
}

int main(int argc, char **argv)
{
    //grabs argv values
    char *shared_lib = argv[1];
    producer_count = strtol(argv[2], NULL, 10);
    consumer_count = strtol(argv[3], NULL, 10);

    char **new_argv = &argv[4];
    int new_argc = argc - 4;
    setlinebuf(stdout);

    //load functions from library
    void *dh = dlopen(shared_lib, RTLD_LAZY);
    run_producer = dlsym(dh, "run_producer");
    run_consumer = dlsym(dh, "run_consumer");
    assign_consumer = dlsym(dh, "assign_consumer");

    pthread_t producers[producer_count];
    pthread_t consumers[consumer_count];

    //array of arguments to be used in startProd and startCon
    struct prodarg_t prodArg[producer_count];
    struct conarg_t conArg[consumer_count];

    //create arrays of size consumer_count for 
    //condition vars, mutex, list heads
    cond = calloc(consumer_count, sizeof(pthread_cond_t));
    lock = calloc(consumer_count, sizeof(pthread_mutex_t));
    heads = calloc(consumer_count, sizeof(struct llist_node));

    //sets args for each producer to use in startProd() 
    //and starts the thread
    for(int i = 0; i < producer_count; i++)
    {
        prodArg[i].num = i;
        prodArg[i].prod_count = producer_count;
        prodArg[i].argc = new_argc;
        prodArg[i].argv = new_argv;
        pthread_create(&producers[i], NULL, startProd, &prodArg[i]);
    }

    //sets args for each consumer to use in startCon()
    //starts consumer threads, inits cond vars and mutexs
    for(int i = 0; i < consumer_count; i++)
    {
        conArg[i].num = i;
        conArg[i].argc = new_argc;
        conArg[i].argv = new_argv;
        pthread_cond_init(&cond[i], NULL);
        pthread_mutex_init(&lock[i], NULL);
        pthread_create(&consumers[i], NULL, startCon, &conArg[i]);
    }

    //waits for producers to end and kills them
    for(int i = 0; i < producer_count; i++)
    {
        pthread_join(producers[i], NULL);
    }

    //builds sentinel node and adds it to the end of each list
    //waits for consumers find sentinel and end then kills them
    for(int i = 0; i < consumer_count; i++)
    {
        struct llist_node *sentinel = malloc(sizeof(struct llist_node));
        sentinel -> str = "";
        sentinel -> next = NULL;
        sentinel -> sentinal = true;
        while(1)
        {
            if(heads[i] == NULL)
            {
                heads[i] = sentinel;
                pthread_cond_signal(&cond[i]);
                break;
            }
        }
        free(sentinel);
        pthread_join(consumers[i], NULL);
    }
    free(cond);
    free(lock);
    return 0;
}