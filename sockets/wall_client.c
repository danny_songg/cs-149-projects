#include <unistd.h> 
#include <stdio.h> 
#include <sys/socket.h> 
#include <stdlib.h> 
#include <netinet/in.h> 
#include <string.h> 
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <time.h>
#include <stdbool.h>
#include <strings.h>
#include <signal.h>
#include <sys/select.h>


static int sockfd;

static short UOK = 10;
static short IMOK = 11;
static short BCAST = 1;
static short MSG = 2;

pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

static bool IMOKReceived = true;
static bool isConnected = true;

void *startSend()
{
    while(isConnected)
    {
        struct timeval timeOut; 
        timeOut.tv_sec = 10;
        timeOut.tv_usec = 0;
        fd_set *readfds = calloc(1, sizeof(fd_set));
        FD_SET(0, readfds);
        int selectRes = select(1, readfds, NULL, NULL, &timeOut);
        if(selectRes == 0)
        {
            //printf("sending UOK\n");
            if(!IMOKReceived)
            {
                printf("Not getting OK messages from server. exiting");
                isConnected = false;
            }
            short sendSize = 4;
            write(sockfd, &sendSize, 2);
            write(sockfd, &UOK, 2);
            IMOKReceived = false;
        }
        else
        {
            char *toSend = NULL;
            size_t len = 0;
            int sendLen = getline(&toSend, &len, stdin);
            if(toSend != 0)
            {
                short sendSize = (short) sendLen + 4;
                write(sockfd, &sendSize, 2);
                write(sockfd, &MSG, 2);
                write(sockfd, toSend, sendLen);
            }
        }
    }    
}

void *startRecieve()
{
    short len;
    read(sockfd, &len, 2);
    while(len > 0)
    {
        short type;
        read(sockfd, &type, 2);
        if(type == BCAST)
        {
            // printf("len: %d type: %d\n", len, type); 
            char *message = calloc(len-4, sizeof(char));
            bzero(message, len-4);
            read(sockfd, message, len-4);
            printf("Recieved: %s\n", message);
        }
        else if(type == IMOK)
        {
            IMOKReceived = true;
            // printf("IMOK recieved\n");
        }
        len = 0;
        read(sockfd, &len, 2);
    }
    printf("disconnected from server\n");
    isConnected = false;
}

int main(int argc, char **argv)
{
    char *hostPort = argv[1];
    char *host = strsep(&hostPort, ":");
    long port = strtol(hostPort, NULL, 10);
    // printf("%s %ld\n", host, port);
    
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sockfd == -1)
    {
        printf("socket creation failed\n");
        exit(0);
    }

    struct sockaddr_in serverAddr;
    serverAddr.sin_family = AF_INET; 
    serverAddr.sin_addr.s_addr = inet_addr(host); 
    serverAddr.sin_port = htons(port); 

    if(connect(sockfd, (struct sockaddr *)&serverAddr, sizeof(serverAddr)) != 0)
    {
        printf("Connect with server failed\n");
        exit(0);
    }    

    pthread_t sendThread;
    pthread_create(&sendThread, NULL, startSend, NULL);
    

    pthread_t recieveThread;
    pthread_create(&recieveThread, NULL, startRecieve, NULL);

    pthread_join(recieveThread, NULL);
}
