#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char **argv)
{
    size_t n = 0;
    char *ptr;
    for(int i = 1; i <= argc; i++)
    {
        long long num;
        if(argc > 1)
        {
            num = atoll(argv[i]);
            if(i+1 == argc)
            {
                i++;
            }
        }
        else if(argc == 1)
        {
            if(getline(&ptr, &n, stdin) != -1)
            {  
                num = atoll(ptr);
                i--;
            }
            else
            {
                break;
            } 
            fflush(stdin);              
        }

        if(num <= 0) 
        {
            printf("A negative or non numeric value has been entered.");
            return -1;
        }

        printf("%lld: ", num);
        fflush(stdout);

        int fds[2];
        pipe(fds);

        pid_t pid = fork();

        int fds1[2];
        pipe(fds1);

        pid_t pid1 = fork();

        if(pid > 0 && pid1 > 0)     //parent  
        {
            printf("I am parent my pid is %d for %lld \n", getpid(), num);
            close(fds[1]);
            close(fds1[1]);
            long long innum;
            int status;
            for(long long n = 1; n < num/8; n ++)        
            {
                if(num % n == 0)
                {
                    printf("%lld ", n);
                }
            }
            
            while(read(fds[0], &innum, 8) != 0)             //reads pipe0 from child1
            { 
                printf("%lld ", innum);
            }
            while(read(fds1[0], &innum, 8) != 0)            //reads pipe1 from child 2
            {
                printf("%lld ", innum);
            }                        
            printf("%lld \n", num);                        
        }
        if(pid == 0)
        {
            if(pid1 > 0)    //child1
            {
                printf("I am child 1 my pid is %d for %lld \n", getpid(), num);
                close(fds1[1]);
                long long innum;
                for (long long m = num*1/8; m < num*2/8; m ++)            
                {   
                    if(num % m == 0)
                    {
                        long long outnum = m;
                        write(fds[1], &outnum, 8);                             //writes pipe0 to parent
                    }
                }
                while(read(fds1[0], &innum, 8) != 0)                            //reads pipe1 from grandchild --- 3/4 - 1
                {
                    write(fds[1], &innum, 8);                                   //writes pipe0 to parent234121324120
                }
                close(fds[1]);  
                exit(getpid());              
            }
            else if(pid1 == 0)      //grandchild
            {
                printf("I am grand my pid is %d for %lld \n", getpid(), num);
                for (long long m = num*2/8; m < num*3/8; m ++)               
                {   
                    if(num % m == 0)
                    {
                        long long outnum = m;
                        write(fds1[1], &outnum, 8);                         //writes pipe1 to child1
                    }
                }
                close(fds1[1]);    
                exit(0);
            }
            exit(0);
        }
        if(pid > 0 && pid1 == 0)  //child2
        {
            printf("I am child 2 my pid is %d for %lld \n", getpid(), num);
            for (long long m = num*3/8; m <= num*4/8; m ++)         //calcs 3/4 - 1 
            {   
                if(num % m == 0)
                {
                    long long outnum = m;
                    write(fds1[1], &outnum, 8);                 //writes pipe1 to parent
                }
            }
            close(fds1[1]);
            exit(0);
        }
    }
}