#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <errno.h>

pid_t childPid;

static void sigHandle(int sig, siginfo_t *si, void *ignore)
{
    if(sig == SIGINT)  
    {
        printf("Signaling %d\n", childPid);
        kill(childPid, sig);
    }
    else if(sig == SIGQUIT) 
    {
        printf("Signaling %d\n", childPid);
        kill(childPid, SIGINT);
        printf("Exiting due to quit signal\n");
        exit(0);
    }
}

int main(int argc, char **argv)
{
    struct sigaction sa;    //borrowed from lecture video
    sa.sa_flags = SA_SIGINFO;
    sigemptyset(&sa.sa_mask);
    sa.sa_sigaction = sigHandle;

    if(sigaction(SIGINT, &sa, NULL) == -1)
    {
        perror("sigaction SIGINT");
        exit(errno);
    }
    else if(sigaction(SIGQUIT, &sa, NULL) == -1)
    {

        perror("sigaction SIGQUIT");
        exit(errno);
    }


    for(int i = 2; i < argc; i++)
    {

        pid_t pid = fork();
        
        char fileName[9];
        snprintf(fileName, 9, "%d.out", i-1);

        if(pid == 0)
        {
            
            close(1);
            close(2);
            
            int fd = open(fileName, O_CREAT | O_RDWR | O_TRUNC, 0666);
            dup(1);

            sa.sa_sigaction = SIG_IGN;
            printf("Executing %s %s \n", argv[1], argv[i]);
            fflush(stdout);

            char *arg[] = {argv[1], argv[i], NULL};
            execvp(argv[1], arg);

            exit(0);
        }

        childPid = pid;

        int cstatus;    //borrowed technique lecture video
        pid_t wpid;
        do
        {
            wpid = wait(&cstatus);
        } while (wpid == -1);

        FILE *fptr = fopen(fileName, "a");

        if(WIFSIGNALED(cstatus))    //used a suggestion from the canvas discussion
        {                           //https://sjsu.instructure.com/courses/1309251/discussion_topics/3622422
            fprintf(fptr, "Stopped excecuting %s %s signal = %d\n", argv[1], argv[i], WTERMSIG(cstatus));
            fflush(stdout);
        }
        else
        {
            fprintf(fptr, "Finished excecuting %s %s exit code = %d\n", argv[1], argv[i], WEXITSTATUS(cstatus));
            fflush(stdout);
        }
        fclose(fptr);
    }
}
